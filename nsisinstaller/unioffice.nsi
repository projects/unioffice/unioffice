;NSIS installer for UniOffice
;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;General

  ;Name and file

  !define NameDist "UniOffice@Etersoft"
  !define VerDist "0.5"

  Name "${NameDist}-v${VerDist}"
  OutFile "${NameDist}-v${VerDist}.exe" 

  ;Default installation folder
  InstallDir "$PROGRAMFILES\UniOffice"

  ;Path for source file
  !define PATHEXCELFROM "..\unioffice_excel\"
  
  ;Request application privileges for Windows Vista and Windows 7
  RequestExecutionLevel admin

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages
  !insertmacro MUI_PAGE_WELCOME 
  !insertmacro MUI_PAGE_LICENSE license.txt
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "Main" SECMAIN

    ;Copy two files
    SetOutPath "$SYSDIR\"
    File "${PATHEXCELFROM}unioffice_excel.tlb"
    File "${PATHEXCELFROM}unioffice_excel.dll"

    ;Registry dll
    ExecWait 'regsvr32.exe /s "$SYSDIR\unioffice_excel.dll"'

    ;Creade directory and copy uninstall files
    CreateDirectory "$INSTDIR"
    WriteUninstaller "$INSTDIR\Uninstall.exe"

    ;Create shortcut for unistall
    CreateDirectory "$SMPROGRAMS\${NameDist}"
    CreateShortCut "$SMPROGRAMS\${NameDist}\Uninstall.lnk" "$INSTDIR\Uninstall.exe"

SectionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"
  ;Unregistered dll
  ExecWait 'regsvr32.exe /s /u "$SYSDIR\unioffice_excel.dll"'

  ;Delete dll
  Delete $SYSDIR\unioffice_excel.tlb
  Delete $SYSDIR\unioffice_excel.dll

  ;Delete shortcut
  Delete $SMPROGRAMS\${NameDist}\Uninstall.lnk
  RMDir  $SMPROGRAMS\${NameDist}

  Delete "$INSTDIR\Uninstall.exe"

  RMDir "$INSTDIR"
SectionEnd